<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $codigo
 * @property string $nombre
 * @property string $apellido1
 * @property string $apellido2
 * @property string $direccion
 * @property string $poblacion
 * @property string $fechanacimiento
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido1', 'apellido2', 'direccion', 'poblacion', 'fechanacimiento'], 'required'],
            [['fechanacimiento'], 'safe'],
            [['nombre', 'apellido1', 'apellido2', 'poblacion'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellido1' => 'Apellido 1',
            'apellido2' => 'Apellido 2',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'fechanacimiento' => 'Fechanacimiento',
        ];
    }
}

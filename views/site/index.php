<?php
/** @var yii\web\View $this */
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-6">
                <h2 class="text-center">Practica 1</h2>
                <p>Crear una tabla de alumnos con una base de datos con los siguientes elementos:</p>
                <ul>
                    <li>Nombre: varchar(100)</li>
                    <li>Apellido1: varchar(100)</li>
                    <li>Apellido2: varchar(100)</li>
                    <li>Direccion: varchar(500)</li>
                    <li>Poblacion: varchar(100)</li>
                    <li>FechaNacimiento: date</li>
                </ul>
            </div>
            <div class="col-6">
                <h2 class="text-center">Pasos a seguir:</h2>
                <ol>
                    <li>Crear la base de datos y la tabla con phpMyadmin.</li>
                    <li>Crear una aplicacion de Yii (composer).</li>
                    <li>Realizar un repositorio local con Git.</li>
                    <li>Configurar la aplicacion
                        <ul>
                            <li>Name.</li>
                            <li>Language.</li>
                            <li>Rutas limpias.</li>
                            <li>Configurar la bbdd.</li>
                        </ul>
                    </li>
                    <li>Limpiar aplicacion.</li>
                    <li>Acceder a gii.
                        <ul>
                            <li>Crear el modelo de la tabla.</li>
                            <li>Crear el CRUD de la tabla.</li>
                        </ul>
                    </li>
                    <li>Ir al layout main y modificar el menu.
                        <ul>
                            <li>Alumnos.</li>
                            <li>Administrar->Accion index.</li>
                            <li>Listar->Accion listar.</li>
                            <li>Crear->Accion create.</li>
                        </ul>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
